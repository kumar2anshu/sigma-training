function myFun(x){
    if(typeof(x)=="string"){
        return "Your input is of string type";
    }
    else{
        return "Your input is not of the type string";
    }
}
console.log(myFun("anshu"));
console.log(myFun(1000));
console.log(myFun({}));
