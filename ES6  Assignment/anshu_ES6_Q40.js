function myFun(x){
    if(typeof(x)==="object"){
        return "Not Primitive";
    }
    else{
        return "Primitive";
    }
}
console.log(myFun("Anshu"));
console.log(myFun(["Anshu","Chethan","Sigma"]));
console.log(myFun("12"));
console.log(myFun([]));