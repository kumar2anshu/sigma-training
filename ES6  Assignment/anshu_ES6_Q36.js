//I am only going to check single character. i.e input is letter or digit or
//small or capital letter

function myFun(x){
    j=x.charCodeAt(0);
    if(j>=65&&j<=90){
        return "Your input is capital alphabet";//i have used ascii value

    }
    else if(j>=97&&j<=122){
        return "Your input is a small alphabet";

    }
    else if(j>=48 && j<=57){
        return "Your input is a digit";

    }
    else{
        return "Your input is a symbol";
    }

}
console.log(myFun("A"));
console.log(myFun("a"));
console.log(myFun("5"));
console.log(myFun("@"));