function validate(){
    var age=document.getElementById("ageValidate");
    var name=document.getElementById("nameValidate");
    var email=document.getElementById("emailValidate");
    var designation=document.getElementById("role");
    var recommend=document.getElementsByName("choice");
    var likes=document.getElementById("like");
    var feedback= document.getElementsByName("improve");
    if(name.value==""){
        alert("First of all Enter Your name:");
        name.focus();
        return false;
    }
    if(age.value<=0){
        alert("Enter valid age");
        age.focus();
        return false;

    }
    if(!(email.value.includes("@",1))){
        alert("Please Enter valid Email");
        email.focus();
        return false;
    }
    if(designation.value=="0"){
        alert("Enter valid field");
        designation.focus();
        return false;
        
    }
    return true;

}

var submitButton=document.getElementById("btn");
submitButton.addEventListener("click",validate);